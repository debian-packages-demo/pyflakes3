# pyflakes3

passive checker of Python 2 and 3 programs

* https://tracker.debian.org/pkg/pyflakes

## License
* https://launchpad.net/pyflakes

## See also
* https://pkgs.alpinelinux.org/packages?name=\*pyflakes\*
